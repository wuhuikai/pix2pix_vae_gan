import torch
import torch.nn as nn

class Updater(object):
    def __init__(self, E_a, E_b, G_a, G_b, D_a, D_b, optimizerG, optimizerD, y_img, input_a, input_b, real_label, fake_label, data_iter_a, data_iter_b, lambda1, lambda2):
        self.E_a, self.E_b, self.G_a, self.G_b, self.D_a, self.D_b = E_a, E_b, G_a, G_b, D_a, D_b
        self.optimizerG, self.optimizerD = optimizerG, optimizerD
        self.y_img, self.input_a, self.input_b, self.real_label, self.fake_label = y_img, input_a, input_b, real_label, fake_label
        self.data_iter_a, self.data_iter_b = data_iter_a, data_iter_b
        self.lambda1, self.lambda2 = lambda1, lambda2

        self.width, self.height = self.y_img.size()

        self.gan_criterion = nn.BCELoss()
        self.l2_criterion = nn.MSELoss(size_average=False)

    def __cat(self, x, batch_size):
        return torch.cat((x, self.y_img.expand(batch_size, 1, self.width, self.height)), 1)

    def forward(self):
        # prepare vars
        data_a, data_b = self.data_iter_a.next(), self.data_iter_b.next()
        batch_size_a, batch_size_b= data_a.size(0), data_b.size(0)
        
        with torch.cuda.device_of(self.input_a):
            self.input_a.data.resize_(data_a.size()).copy_(data_a)
            self.input_b.data.resize_(data_b.size()).copy_(data_b)
        # x-->E-->z-->G-->x'
        x_a = self.__cat(self.input_a, batch_size_a)
        x_b = self.__cat(self.input_b, batch_size_b)

        z_a, z_b = self.E_a(x_a), self.E_b(x_b)
        x_a_a, x_b_a, x_a_b, x_b_b = self.G_a(z_a), self.G_a(z_b), self.G_b(z_a), self.G_b(z_b)
        
        x_a_a = self.__cat(x_a_a, batch_size_a)
        x_b_a = self.__cat(x_b_a, batch_size_b)
        x_a_b = self.__cat(x_a_b, batch_size_a)
        x_b_b = self.__cat(x_b_b, batch_size_b)

        return x_a, x_b, z_a, z_b, x_a_a, x_b_a, x_a_b, x_b_b

    def __update_d(self, D, x, x_a, x_b):
        # train with real
        output = D(x.detach())
        errD_real = self.gan_criterion(output, self.real_label.expand_as(output.data))

        # train with fake
        output = D(x_a.detach())
        errD_fake_a = self.gan_criterion(output, self.fake_label.expand_as(output.data))
        
        output = D(x_b.detach())
        errD_fake_b = self.gan_criterion(output, self.fake_label.expand_as(output.data))

        errD = errD_real + 0.5*errD_fake_a + 0.5*errD_fake_b
        
        return errD

    def update_D(self):
        ############################
        # (1) Update D network: maximize log(D(x)) + 0.5*log(1 - D(G_a(z_a))) + 0.5*log(1 - D(G_a(z_b)))
        ###########################
        # prepare data and forward through E, G
        x_a, x_b, _, _, x_a_a, x_b_a, x_a_b, x_b_b = self.forward()

        # update
        self.D_a.zero_grad()
        self.D_b.zero_grad()
        err_D_a = self.__update_d(self.D_a, x_a, x_a_a, x_b_a)
        err_D_b = self.__update_d(self.D_b, x_b, x_a_b, x_b_b)
        errD = err_D_a + err_D_b
        errD.backward()
        self.optimizerD.step()

        return errD

    def __update_g(self, D, x_a, x_b, x, z):
        # GAN loss
        output = D(x_a)
        errG_a = self.gan_criterion(output, self.real_label.expand_as(output.data))
        output = D(x_b)
        errG_b = self.gan_criterion(output, self.real_label.expand_as(output.data))
        errG = 0.5*errG_a + 0.5*errG_b

        # VAE l2 loss
        l2_loss = 0.5*self.l2_criterion(x_a, x)

        # VAE KL loss - 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        kl_loss = 0
        for _, mu, sigma in z:
            kl_loss += torch.mean(mu.pow(2).add_(sigma.exp()).mul_(-1).add_(1).add_(sigma)).mul_(-0.5)

        return errG, l2_loss, kl_loss

    def update_G(self):
        ############################
        # (2) Update G network: minimize -log(D(G(z))) + lambda1*||x-x'|| + lambda2*KL
        ###########################
        x_a, x_b, z_a, z_b, x_a_a, x_b_a, x_a_b, x_b_b = self.forward()        

        # update
        self.E_a.zero_grad()
        self.E_b.zero_grad()
        self.G_a.zero_grad()
        self.G_b.zero_grad()
        errG_a, l2_a, kl_a = self.__update_g(self.D_a, x_a_a, x_b_a, x_a, z_a)
        errG_b, l2_b, kl_b = self.__update_g(self.D_b, x_b_b, x_a_b, x_b, z_b)

        # Loss
        errG, l2, kl = errG_a + errG_b, l2_a + l2_b, kl_a + kl_b
        err = errG + self.lambda1*l2 + self.lambda2*kl
        err.backward()
        self.optimizerG.step()

        return err, errG, l2, kl