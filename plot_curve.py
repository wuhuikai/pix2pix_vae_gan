import os
import json
import argparse

import numpy as np

from scipy.ndimage.filters import median_filter

from matplotlib import pyplot as plt

def main():
    parser = argparse.ArgumentParser("Plot training curves")
    parser.add_argument('--experiment', default='results', help='Path for storing models and log')
    parser.add_argument('--step_size', default=1, type=int, help='Plot a point every [step_size] iters')
    parser.add_argument('--skip', default=0, type=int, help='Skip first [skip] iters')
    parser.add_argument('--smooth_size', default=1, type=int, help='Window size for smooth, 1 indicate no smooth')
    opt = parser.parse_args()

    with open(os.path.join(opt.experiment, 'log')) as f:
        inputs = ''.join(f.readlines()).replace('\n', '').replace('\'', '"')
        records = json.loads(inputs)
    
    index = list(range(opt.skip, len(records), opt.step_size))
    records = records[opt.skip::opt.step_size]

    d_loss = median_filter([record['err_D'] for record in records], size=opt.smooth_size)
    g_loss = median_filter([record['err_G'] for record in records], size=opt.smooth_size)
    plt.figure()
    plt.plot(index, d_loss, label='err_D')
    plt.plot(index, g_loss, label='err_G')
    plt.legend()
    plt.savefig(os.path.join(opt.experiment, 'err_G_D.png'))

    l2_loss = median_filter([record['err_L2'] for record in records], size=opt.smooth_size)
    plt.figure()
    plt.plot(index, l2_loss, label='L2')
    plt.legend()
    plt.savefig(os.path.join(opt.experiment, 'L2.png'))
    
    kl_loss = median_filter([record['err_KL'] for record in records], size=opt.smooth_size)
    plt.figure()
    plt.plot(index, kl_loss, label='KL')
    plt.legend()
    plt.savefig(os.path.join(opt.experiment, 'KL.png'))

    gan_vae_loss = median_filter([record['err_GAN_VAE'] for record in records], size=opt.smooth_size)
    plt.figure()
    plt.plot(index, gan_vae_loss, label='GAN_VAE')
    plt.legend()
    plt.savefig(os.path.join(opt.experiment, 'GAN_VAE.png'))

if __name__ == '__main__':
    main()