import os

from PIL import Image

import torch.utils.data as data

def default_loader(path):
    return Image.open(path).convert('RGB')

class ImageList(data.Dataset):
    def __init__(self, root, list_path, transform=None, loader=default_loader):
        with open(list_path) as f:
            imgs = list(f)
        imgs = [os.path.join(root, p.strip()) for p in imgs]
        
        self.imgs = imgs
        self.transform = transform
        self.loader = loader

    def __getitem__(self, index):
        path = self.imgs[index]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)

        return img

    def __len__(self):
        return len(self.imgs)

class DataItereator(object):
    def __init__(self, data_loader):
        self.data_loader = data_loader

        self.iterator = iter(data_loader)
        self.length = len(self.iterator)
        self.ptr = 0        

    def __len__(self):
        return self.length

    def current_ptr(self):
        return self.ptr

    def next(self):
        if self.ptr == self.length:
            self.ptr = 0
            self.iterator = iter(self.data_loader)

        self.ptr += 1
        return next(self.iterator)