from __future__ import print_function

import os
import random
import argparse

import numpy as np

train_set = set(['set00', 'set01', 'set02', 'set03', 'set04', 'set05'])
test_set =  set(['set06', 'set07', 'set08', 'set09', 'set10', 'set11'])

day_set =   set(['set00', 'set01', 'set02', 'set06', 'set07', 'set08'])
night_set = set(['set03', 'set04', 'set05', 'set09', 'set10', 'set11'])

type_to_name = {'ir': set(['lwir',]), 'rgb': set(['visible',]), 'all': set(['lwir', 'visible'])}

def main():
    parser = argparse.ArgumentParser("Generate specified image list")
    parser.add_argument('--dataroot', default='/data1/wuhuikai/benchmark/KAIST', help='Path to dataset')
    parser.add_argument('--train_or_test', default='all', help='Traing set | testing set | training set+testing set: train|test|all')
    parser.add_argument('--day_or_night', default='all', help='Day set | night set | day set+night set: day|night|all')
    parser.add_argument('--ir_or_rgb', default='all', help='IR set | RGB set | IR set+RGB set: ir|rgb|all')
    parser.add_argument('--select_n_samples', default=0, type=int, help='# of samples to select, select all samples if 0')
    parser.add_argument('--manual_seed', type=int, help='Manual seed')

    parser.add_argument('--out_path', default='.', help='Folder path for storing output image list')

    opt = parser.parse_args()
    if opt.manual_seed:
        np.random.seed(opt.manual_seed)

    if opt.train_or_test == 'train':
        folder_set = set(train_set)
    elif opt.train_or_test == 'test':
        folder_set = set(test_set)
    else:
        opt.train_or_test = 'all'
        folder_set = train_set | test_set

    if opt.day_or_night == 'day':
        folder_set &= day_set
    elif opt.day_or_night == 'night':
        folder_set &= night_set
    else:
        opt.day_or_night == 'all'
        folder_set &= (day_set | night_set)

    if opt.ir_or_rgb not in ('ir', 'rgb', 'all'):
        opt.ir_or_rgb = 'all'
    type_name = type_to_name[opt.ir_or_rgb]

    print('train|test|all: {}\tday|night|all: {}\tir|rgb|all: {}'.format(opt.train_or_test, opt.day_or_night, opt.ir_or_rgb))
    print('\tselected subfolder: {}, selected type: {}\n'.format(sorted(folder_set), sorted(type_name)))

    print('Start generate image list, root folder:{} ...'.format(opt.dataroot))
    result = []
    for idx, subfolder in enumerate(folder_set):
        print('\tProcessing {}/{} ...'.format(idx+1, len(folder_set)))
        path = os.path.join(opt.dataroot, subfolder)
        if not os.path.isdir(path):
            print('\t\tWarning: {} is not a valid folder path !!!'.format(path))
            continue
        dirs = os.listdir(path)
        if len(dirs) == 0:
            print('\t\tWarning: {} is empty !!!'.format(path))
            continue

        if dirs[0].startswith('V'):
            dirs = [os.path.join(subfolder, d, sub_folder) for d in dirs for sub_folder in os.listdir(os.path.join(path, d)) if sub_folder in type_name]
        else:
            dirs = [os.path.join(subfolder, d) for d in dirs if d in type_name]

        sub_result = [os.path.join(d, path) for d in dirs for path in os.listdir(os.path.join(opt.dataroot, d))]
        result += sub_result
        print('\t\tTotal {} images selected\n'.format(len(sub_result)))

    if opt.select_n_samples > 0 and opt.select_n_samples < len(result):
        print('Select {} samples from {} samples in total\n'.format(opt.select_n_samples, len(result)))
        result = np.random.choice(sorted(result), size=opt.select_n_samples, replace=False)

    out_path = os.path.join(opt.out_path, '{}_{}_{}_{}_seed_{}.txt'.format(opt.train_or_test, opt.day_or_night, opt.ir_or_rgb, len(result), opt.manual_seed))
    print('Write total {} images to list: {} ...'.format(len(result), out_path))
    with open(out_path, 'w') as f:
        f.write('\n'.join(result))

if __name__ == '__main__':
    main()