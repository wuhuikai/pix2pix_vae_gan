from __future__ import print_function

import os
import time
import random
import pprint
import argparse

import numpy as np

import torch
import torch.utils.data
import torch.optim as optim
import torch.backends.cudnn as cudnn

from torch.autograd import Variable

import torchvision.utils as vutils
import torchvision.transforms as transforms

from updater import Updater
from dataset import ImageList, DataItereator
from model import Encoder, Generator, Discriminator, weights_init

def main():
    parser = argparse.ArgumentParser("Unsupervised Image-to-Image Translation")
    parser.add_argument('--dataroot_a', default='/data1/wuhuikai/benchmark/KAIST', help='Path to dataset a')
    parser.add_argument('--dataroot_b', default='/data1/wuhuikai/benchmark/KAIST', help='Path to dataset b')
    parser.add_argument('--img_list_a', default='list/all_day_rgb_54768.txt', help='Image list for dataset a')
    parser.add_argument('--img_list_b', default='list/all_night_rgb_28657.txt', help='Image list for dataset b')
    parser.add_argument('--workers', type=int, help='# of workers for data loading', default=2)
    parser.add_argument('--batch_size', type=int, default=2, help='Input batch size')
    parser.add_argument('--image_size', type=int, nargs='+', default=(512, 640), help='The (height,width) of the input image to network, --image_size [width] [height](optional)')
    parser.add_argument('--n_unshared_weights', type=int, default=2, help='# of layers that do not share weights for E, G and D')
    parser.add_argument('--n_unet_layers', type=int, default=4, help='# of layers pass from E to G')
    parser.add_argument('--nef', type=int, default=64, help='# of channels for the 1st layer of Encoder')
    parser.add_argument('--ngf', type=int, default=64, help='# of channels for the 1st layer of Generator')
    parser.add_argument('--ndf', type=int, default=64, help='# of channels for the 1st layer of Discriminator')
    parser.add_argument('--n_layers', type=int, default=6, help='# of layers in E, G')
    parser.add_argument('--n_layers_d', type=int, default=6, help='# of layers in D')
    parser.add_argument('--n_epoch', type=int, default=10, help='# of epochs to train for')
    parser.add_argument('--lr', type=float, default=0.0002, help='Learning rate, default=0.0002')
    parser.add_argument('--beta1', type=float, default=0.5, help='Beta1 for adam. default=0.5')
    parser.add_argument('--lambda1', type=float, default=100, help='Lambda1 for G loss')
    parser.add_argument('--lambda2', type=float, default=1, help='Lambda2 for G loss')
    parser.add_argument('--d_iters', type=int, default=1, help='# of D iters per each G iter')
    parser.add_argument('--gpu_id', type=int, default=0, help='Which GPU to use, -1 for CPU')
    parser.add_argument('--net_path_template', default='', help="Template for the path of E, G and D (to continue training)")
    parser.add_argument('--experiment', default='results', help='Folder to output images and model checkpoints')
    parser.add_argument('--manual_seed', type=int, help='Manual seed')
    parser.add_argument('--print_intervel', type=int, default=10, help='Print infos every [print_intervel] iterations')
    parser.add_argument('--test_intervel', type=int, default=100, help='Test every [test_intervel] iterations')
    parser.add_argument('--save_intervel', type=int, default=1000, help='Save trained models every [save_intervel] iterations')

    # parse arguments
    opt = parser.parse_args()
    if len(opt.image_size) == 1:
        opt.image_size *= 2
    opt.image_size = tuple(opt.image_size)
    total_pixel = opt.image_size[0]*opt.image_size[1]*3
    opt.lambda1 /= total_pixel

    print('Input arguments:')
    for key, value in sorted(vars(opt).items()):
        print('\t{}: {}'.format(key, value))
    print('')

    # mkdir for models and output results
    preview_path = os.path.join(opt.experiment, 'preview')
    if not os.path.isdir(preview_path):
        os.makedirs(preview_path)
    print('Saving results to folder: "{}" ...\n'.format(opt.experiment))

    # set random seed
    if opt.manual_seed is None:
        opt.manual_seed = random.randint(1, 10000)
    random.seed(opt.manual_seed)
    torch.manual_seed(opt.manual_seed)
    if opt.gpu_id >= 0:
        torch.cuda.manual_seed_all(opt.manual_seed)
    print("Random Seed: {}\n".format(opt.manual_seed))

    with open(os.path.join(opt.experiment, 'opt'), 'w') as f:
        f.write(pprint.pformat(vars(opt)))

    # use cudnn
    cudnn.benchmark = True

    # load dataset
    print('Loading dataset ...')
    scale_size = opt.image_size[::-1]
    dataset_a = ImageList(root=opt.dataroot_a, list_path=opt.img_list_a, transform=transforms.Compose([
                                   transforms.Scale(scale_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                               ]))
    dataloader_a = torch.utils.data.DataLoader(dataset_a, batch_size=opt.batch_size, shuffle=True, num_workers=opt.workers)

    dataset_b = ImageList(root=opt.dataroot_b, list_path=opt.img_list_b, transform=transforms.Compose([
                                   transforms.Scale(scale_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                               ]))
    dataloader_b = torch.utils.data.DataLoader(dataset_b, batch_size=opt.batch_size, shuffle=True, num_workers=opt.workers)
    print('\tLoad done, dataset A has {} images, dataset B has {} images\n'.format(len(dataset_a), len(dataset_b)))

    # Init model
    E_shared_weights = [False]*opt.n_unshared_weights + [True]*(opt.n_layers-opt.n_unshared_weights)
    E_unet_layers = [False]*(opt.n_layers-opt.n_unet_layers) + [True]*opt.n_unet_layers
    E_a = Encoder(4, opt.nef, opt.n_layers, unet_layers=E_unet_layers, shared_weights=E_shared_weights)
    E_b = Encoder(4, opt.nef, opt.n_layers, unet_layers=E_unet_layers, shared_weights=E_shared_weights, ref_net=E_a)

    G_shared_weights = [True]*(opt.n_layers-opt.n_unshared_weights) + [False]*opt.n_unshared_weights
    G_unet_layers = [True]*opt.n_unet_layers + [False]*(opt.n_layers-opt.n_unet_layers)
    G_a = Generator(3, opt.ngf, opt.n_layers, unet_layers=G_unet_layers, shared_weights=G_shared_weights)
    G_b = Generator(3, opt.ngf, opt.n_layers, unet_layers=G_unet_layers, shared_weights=G_shared_weights, ref_net=G_a)

    D_shared_weights = [False]*opt.n_unshared_weights + [True]*(opt.n_layers_d-opt.n_unshared_weights)
    D_a = Discriminator(4, opt.ndf, opt.n_layers_d, shared_weights=D_shared_weights)
    D_b = Discriminator(4, opt.ndf, opt.n_layers_d, shared_weights=D_shared_weights, ref_net=D_a)

    if opt.net_path_template:
        with torch.cuda.device(opt.gpu_id):
            template = os.path.join(opt.experiment, opt.net_path_template)
            print('Loading pretrained models from {} ...'.format(template))
            E_a.load_state_dict(torch.load(template.format('E_a'), map_location=lambda storage, loc: storage))
            E_b.load_state_dict(torch.load(template.format('E_b'), map_location=lambda storage, loc: storage))
            G_a.load_state_dict(torch.load(template.format('G_a'), map_location=lambda storage, loc: storage))
            G_b.load_state_dict(torch.load(template.format('G_b'), map_location=lambda storage, loc: storage))
            D_a.load_state_dict(torch.load(template.format('D_a'), map_location=lambda storage, loc: storage))
            D_b.load_state_dict(torch.load(template.format('D_b'), map_location=lambda storage, loc: storage))
            print('\tLoading pretrained models done!\n')
    else:        
        print('Init model weights ...')
        E_a.apply(weights_init)
        E_b.apply(weights_init)
        G_a.apply(weights_init)
        G_b.apply(weights_init)
        D_a.apply(weights_init)
        D_b.apply(weights_init)
        print('\tWeights init done!\n')

    # Init user vars
    y_img = torch.linspace(-1, 1, steps=opt.image_size[0]).repeat(opt.image_size[1], 1).t_()
    input_a = torch.FloatTensor(opt.batch_size, 3, *opt.image_size)
    input_b = torch.FloatTensor(opt.batch_size, 3, *opt.image_size)
    real_label = torch.FloatTensor([1]).fill_(1)
    fake_label = torch.FloatTensor([1]).fill_(0)
    idx_a, idx_b = np.random.choice(range(len(dataset_a))), np.random.choice(range(len(dataset_b)))
    test_img_a = dataset_a[idx_a].unsqueeze(0)
    test_img_b = dataset_b[idx_b].unsqueeze(0)

    # Transfer to GPU
    if opt.gpu_id >= 0:
        print('Transfer models and tensors to GPU {} ...'.format(opt.gpu_id))
        with torch.cuda.device(opt.gpu_id):
            E_a.cuda()
            E_b.cuda()
            G_a.cuda()
            G_b.cuda()
            D_a.cuda()
            D_b.cuda()

            y_img, input_a, input_b = y_img.cuda(), input_a.cuda(), input_b.cuda()
            real_label, fake_label = real_label.cuda(), fake_label.cuda()
            test_img_a, test_img_b = test_img_a.cuda(), test_img_b.cuda()
        print('\tTransfer to GPU done!\n')

    # Tensors to Vars
    y_img, input_a, input_b = Variable(y_img, requires_grad=False), Variable(input_a, requires_grad=False), Variable(input_b, requires_grad=False)
    real_label, fake_label = Variable(real_label, requires_grad=False), Variable(fake_label, requires_grad=False)
    test_img_a, test_img_b = Variable(test_img_a, volatile=True), Variable(test_img_b, volatile=True)

    # setup optimizer
    optimizerG = optim.Adam(set(E_a.parameters()) | set(E_b.parameters()) | set(G_a.parameters()) | set(G_b.parameters())
        , lr=opt.lr, betas=(opt.beta1, 0.999))
    optimizerD = optim.Adam(set(D_a.parameters()) | set(D_b.parameters()), lr=opt.lr, betas=(opt.beta1, 0.999))

    # Prepare dataloader iterator and updater
    data_iter_a, data_iter_b = DataItereator(dataloader_a), DataItereator(dataloader_b)
    iters_per_epoch = max(len(data_iter_a), len(data_iter_b))
    updater = Updater(E_a, E_b, G_a, G_b, D_a, D_b, optimizerG, optimizerD, y_img, input_a, input_b, real_label, fake_label, data_iter_a, data_iter_b, opt.lambda1, opt.lambda2)

    # Main Loop
    print('Training start ...\n')
    total_iters = opt.n_epoch*iters_per_epoch
    infos = []
    start = time.time()
    for i in range(total_iters):
        epoch, n_iter = i//iters_per_epoch, i%iters_per_epoch

        # Train
        for k in range(opt.d_iters):
            err_D = updater.update_D()
        
        err, err_G, l2, kl = updater.update_G()

        # Save Infos
        infos.append({'epoch': epoch, 'n_iter': n_iter, 'err_D': err_D.data[0], 'err_GAN_VAE': err.data[0], 'err_G': err_G.data[0], 'err_L2': l2.data[0], 'err_KL': kl.data[0]})

        # Plot infos
        i += 1
        if i % opt.print_intervel == 0:
            print('[%d/%d][%d/%d] Loss_D: %.4f Loss_GAN_VAE: %.4f Loss_G: %.4f Loss_L2: %.4f Loss_KL: %.4f'
              % (epoch, opt.n_epoch, n_iter, iters_per_epoch, err_D.data[0], err.data[0], err_G.data[0], l2.data[0], kl.data[0]))
            iters_per_sec = i/(time.time() - start)
            min_to_finish = (total_iters-i) / iters_per_sec / 60
            days, rest_mins = min_to_finish//(60*24), min_to_finish%(60*24)
            print('{} iters/s, finish in {} days, {} hours, {} minutes'.format(iters_per_sec, days, rest_mins//60, rest_mins%60), end='\r')

        if i % opt.save_intervel == 0 or i == total_iters:
            torch.save(E_a.state_dict(), '%s/E_a_iter_%d.pth' % (opt.experiment, i))
            torch.save(E_b.state_dict(), '%s/E_b_iter_%d.pth' % (opt.experiment, i))
            torch.save(G_a.state_dict(), '%s/G_a_iter_%d.pth' % (opt.experiment, i))
            torch.save(G_b.state_dict(), '%s/G_b_iter_%d.pth' % (opt.experiment, i))
            torch.save(D_a.state_dict(), '%s/D_a_iter_%d.pth' % (opt.experiment, i))
            torch.save(D_b.state_dict(), '%s/D_b_iter_%d.pth' % (opt.experiment, i))

            with open(os.path.join(opt.experiment, 'log'), 'w') as f:
                f.write(pprint.pformat(infos))

        if i % opt.test_intervel == 0 or i == total_iters:
            E_a.eval(), E_b.eval(), G_a.eval(), G_b.eval()

            z_a = E_a(torch.cat((test_img_a, y_img.expand(1, 1, *opt.image_size)), 1), use_mean=True)
            z_b = E_b(torch.cat((test_img_b, y_img.expand(1, 1, *opt.image_size)), 1), use_mean=True)
            test_a_a, test_a_b, test_b_a, test_b_b = G_a(z_a), G_b(z_a), G_a(z_b), G_b(z_b)
            save_img = torch.cat([img.expand(1, 3, *opt.image_size) for img in (test_img_a, test_img_b, test_a_a, test_a_b, test_b_a, test_b_b)], 0)

            vutils.save_image(save_img.data, '%s/preview/real_samples_iter_%d.png' % (opt.experiment, i),  nrow=2, normalize=True, scale_each=True)

            E_a.train(), E_b.train(), G_a.train(), G_b.train()

if __name__ == '__main__':
    main()