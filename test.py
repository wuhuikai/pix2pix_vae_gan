from __future__ import print_function

import os
import random
import argparse

import numpy as np

import torch
import torch.backends.cudnn as cudnn

from torch.autograd import Variable

import torchvision.utils as vutils
import torchvision.transforms as transforms

from dataset import ImageList
from model import Encoder, Generator

def main():
    parser = argparse.ArgumentParser("Unsupervised Image-to-Image Translation --- TEST")
    parser.add_argument('--net_path_template', required=True, help="Template for the path of E and G(to test)")
    parser.add_argument('--dataroot_a', default='/data1/wuhuikai/benchmark/KAIST', help='Path to dataset a')
    parser.add_argument('--dataroot_b', default='/data1/wuhuikai/benchmark/KAIST', help='Path to dataset b')
    parser.add_argument('--img_list_a', default='list/all_day_rgb_54768.txt', help='Image list for dataset a')
    parser.add_argument('--img_list_b', default='list/all_night_rgb_28657.txt', help='Image list for dataset b')
    parser.add_argument('--image_size', type=int, nargs='+', default=(512, 640), help='The (height,width) of the input image to network, --image_size [width] [height](optional)')
    parser.add_argument('--n_unet_layers', type=int, default=4, help='# of layers pass from E to G')
    parser.add_argument('--nef', type=int, default=64, help='# of channels for the 1st layer of Encoder')
    parser.add_argument('--ngf', type=int, default=64, help='# of channels for the 1st layer of Generator')
    parser.add_argument('--use_mean', action='store_true', default=False, help='Use mean only if True')
    parser.add_argument('--n_layers', type=int, default=6, help='# of layers in E and G')
    parser.add_argument('--test_iters', type=int, default=35, help='# of images to test')
    parser.add_argument('--gpu_id', type=int, default=0, help='Which GPU to use, -1 for CPU')
    parser.add_argument('--experiment', default='results', help='Folder which stores model checkpoints')
    parser.add_argument('--manual_seed', type=int, help='Manual seed')
    
    # parse arguments
    opt = parser.parse_args()
    if len(opt.image_size) == 1:
        opt.image_size *= 2
    opt.image_size = tuple(opt.image_size)

    print('Input arguments:')
    for key, value in sorted(vars(opt).items()):
        print('\t{}: {}'.format(key, value))
    print('')

    # mkdir for storing test results
    test_path = os.path.join(opt.experiment, 'test')
    if not os.path.isdir(test_path):
        os.makedirs(test_path)
    print('Saving test results to folder: "{}" ...\n'.format(test_path))

    # set random seed
    if opt.manual_seed is None:
        opt.manual_seed = random.randint(1, 10000)
    random.seed(opt.manual_seed)
    torch.manual_seed(opt.manual_seed)
    if opt.gpu_id >= 0:
        torch.cuda.manual_seed_all(opt.manual_seed)
    print("Random Seed: {}\n".format(opt.manual_seed))

    # use cudnn
    cudnn.benchmark = True

    # load dataset
    print('Loading dataset ...')
    scale_size = opt.image_size[::-1]
    dataset_a = ImageList(root=opt.dataroot_a, list_path=opt.img_list_a, transform=transforms.Compose([
                                   transforms.Scale(scale_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                               ]))

    dataset_b = ImageList(root=opt.dataroot_b, list_path=opt.img_list_b, transform=transforms.Compose([
                                   transforms.Scale(scale_size),
                                   transforms.ToTensor(),
                                   transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                               ]))
    print('\tLoad done, dataset A has {} images, dataset B has {} images\n'.format(len(dataset_a), len(dataset_b)))
    
    # Init model
    E_unet_layers = [False]*(opt.n_layers-opt.n_unet_layers) + [True]*opt.n_unet_layers
    E_a = Encoder(4, opt.nef, opt.n_layers, unet_layers=E_unet_layers)
    E_b = Encoder(4, opt.nef, opt.n_layers, unet_layers=E_unet_layers)

    G_unet_layers = [True]*opt.n_unet_layers + [False]*(opt.n_layers-opt.n_unet_layers)
    G_a = Generator(3, opt.ngf, opt.n_layers, unet_layers=G_unet_layers)
    G_b = Generator(3, opt.ngf, opt.n_layers, unet_layers=G_unet_layers)

    with torch.cuda.device(opt.gpu_id):
        template = os.path.join(opt.experiment, opt.net_path_template)
        print('Loading pretrained models from {} ...'.format(template))
        E_a.load_state_dict(torch.load(template.format('E_a'), map_location=lambda storage, loc: storage))
        E_b.load_state_dict(torch.load(template.format('E_b'), map_location=lambda storage, loc: storage))
        G_a.load_state_dict(torch.load(template.format('G_a'), map_location=lambda storage, loc: storage))
        G_b.load_state_dict(torch.load(template.format('G_b'), map_location=lambda storage, loc: storage))
        print('\tLoading pretrained models done!\n')

    # Init user vars
    y_img = torch.linspace(-1, 1, steps=opt.image_size[0]).repeat(opt.image_size[1], 1).t_().expand(1, 1, *opt.image_size)
    input_a = torch.FloatTensor(1, 3, *opt.image_size)
    input_b = torch.FloatTensor(1, 3, *opt.image_size)
    
    # Transfer to GPU
    if opt.gpu_id >= 0:
        print('Transfer models and tensors to GPU {} ...'.format(opt.gpu_id))
        with torch.cuda.device(opt.gpu_id):
            E_a.cuda()
            E_b.cuda()
            G_a.cuda()
            G_b.cuda()

            y_img, input_a, input_b = y_img.cuda(), input_a.cuda(), input_b.cuda()
        print('\tTransfer to GPU done!\n')

    # Tensors to Vars
    y_img, input_a, input_b = Variable(y_img, volatile=True), Variable(input_a, volatile=True), Variable(input_b, volatile=True)
    
    # Main Loop
    print('Testing start ...\n')
    E_a.eval(), E_b.eval(), G_a.eval(), G_b.eval()
    
    for i in range(opt.test_iters):
        print('Processing {}/{} ...'.format(i+1, opt.test_iters))

        idx_a, idx_b = np.random.choice(range(len(dataset_a))), np.random.choice(range(len(dataset_b)))
        img_a = dataset_a[idx_a].unsqueeze(0)
        img_b = dataset_b[idx_b].unsqueeze(0)
        with torch.cuda.device(opt.gpu_id):
            input_a.data.copy_(img_a)
            input_b.data.copy_(img_b)
        
        z_a, z_b = E_a(torch.cat((input_a, y_img), 1), use_mean=opt.use_mean), E_b(torch.cat((input_b, y_img), 1), use_mean=opt.use_mean)
        test_a_a, test_a_b, test_b_a, test_b_b = G_a(z_a), G_b(z_a), G_a(z_b), G_b(z_b)

        save_img = torch.cat([img.expand(1, 3, *opt.image_size) for img in (input_a, input_b, test_a_a, test_a_b, test_b_a, test_b_b)], 0)
        vutils.save_image(save_img.data, '%s/test_samples_%d.png' % (test_path, i),  nrow=2, normalize=True, scale_each=True)

if __name__ == '__main__':
    main()