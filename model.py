from __future__ import print_function

import collections

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

# custom weights initialization
def weights_init(m):
    class_name = m.__class__.__name__
    if 'Conv' in class_name:
        m.weight.data.normal_(0.0, 0.02)
    elif 'BatchNorm' in class_name:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

class VAE(nn.Module):
    def __init__(self, in_channels, out_channels, bias):
        super(VAE, self).__init__()

        self.mu = nn.Conv2d(in_channels, out_channels, 1, stride=1, bias=bias)
        self.sigma = nn.Conv2d(in_channels, out_channels, 1, stride=1, bias=bias)

    def reparametrize(self, mu, sigma, use_mean=False):
        if use_mean:
            return mu

        std = sigma.mul(0.5).exp_()
        if isinstance(mu.data, torch.cuda.FloatTensor):
            with torch.cuda.device_of(mu.data):
                eps = torch.cuda.FloatTensor(mu.size()).normal_()
        else:
            eps = torch.FloatTensor(mu.size()).normal_()
        eps = Variable(eps, requires_grad=False)

        return eps.mul(std).add_(mu)

    def forward(self, input, use_mean=False):
        mu = self.mu(input)
        sigma = self.sigma(input)
        # print('[mu: {}, sigma: {}]'.format(tuple(mu.size()), tuple(sigma.size())), end='')

        return self.reparametrize(mu, sigma, use_mean), mu, sigma

class VAE_Block(nn.Module):
    def __init__(self, in_channels, out_channels, ks, stride, padding, bias, relu_func, vae=False):
        super(VAE_Block, self).__init__()

        self.conv = nn.Conv2d(in_channels, out_channels, ks, stride=stride, padding=padding, bias=bias)
        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = relu_func()
        self.vae = VAE(in_channels, out_channels, bias) if vae else None
        
    def forward(self, input, use_mean=False):
        output = self.relu(self.bn(self.conv(input)))
        # print('--->{}'.format(tuple(output.size())), end='')
        if self.vae:
            return output, self.vae(input, use_mean)
        
        return output

class Encoder(nn.Module):
    def __init__(self, nc, nef, n_layers, unet_layers=None, shared_weights=None, ref_net=None, ks=4, stride=2, padding=1, bias=False, slope=0.2, max_c=2048):
        super(Encoder, self).__init__()

        if not isinstance(nef, collections.Iterable):
            nef = [min(nef*(2**i), max_c)  for i in range(n_layers)]
        assert(len(nef) == n_layers)
        assert(shared_weights == None or len(shared_weights) == n_layers)
        assert(unet_layers == None or len(unet_layers) == n_layers)

        in_channels = nc
        for idx, out_channels in enumerate(nef):
            layer_id = str(idx+1)

            if ref_net and shared_weights and shared_weights[idx]:
                self.add_module(layer_id, ref_net[layer_id])
                continue

            if idx == n_layers-1:
                self.add_module(layer_id, VAE(in_channels, out_channels, bias))
            else:
                self.add_module(layer_id, VAE_Block(in_channels, out_channels, ks, stride, padding, bias, lambda:nn.LeakyReLU(slope), unet_layers and unet_layers[idx]))

            in_channels = out_channels

        self.n_layers = n_layers
        self.unet_layers = unet_layers

    def __getitem__(self, key):
        return getattr(self, key)

    def forward(self, input, use_mean=False):
        output = []

        # print(tuple(input.size()), end='')
        for idx in range(self.n_layers-1):
            layer_id = str(idx+1)
            if self.unet_layers and self.unet_layers[idx]:
                input, vae_output = self[layer_id](input, use_mean)
                output.append(vae_output)
            else:
                input = self[layer_id](input)

        # print('--->', end='')
        layer_id = str(self.n_layers)
        output.append(self[layer_id](input))
        output.reverse()
        # print('\n')

        return output

class Generator(nn.Module):
    def __init__(self, nc, ngf, n_layers, unet_layers=None, shared_weights=None, ref_net=None, ks=4, stride=2, padding=1, bias=False, slope=0.2, max_c=2048):
        super(Generator, self).__init__()

        if not isinstance(ngf, collections.Iterable):
            ngf = [min(ngf*(2**i), max_c)  for i in range(n_layers)]
            ngf.reverse()
        assert(len(ngf) == n_layers)
        assert(shared_weights == None or len(shared_weights) == n_layers)
        assert(unet_layers == None or len(unet_layers) == n_layers)

        ngf.append(nc)
        for idx, in_channels in enumerate(ngf[:-1]):
            layer_id = str(idx+1)
            out_channels = ngf[idx+1]

            if ref_net and shared_weights and shared_weights[idx]:
                self.add_module(layer_id, ref_net[layer_id])
                continue

            if idx == n_layers-1:
                self.add_module(layer_id, 
                    nn.Sequential(
                        nn.ConvTranspose2d(in_channels, out_channels, 1, stride=1, bias=bias),
                        nn.Tanh()
                    ))
            else:
                self.add_module(layer_id,
                    nn.Sequential(
                        nn.ConvTranspose2d(in_channels, out_channels, ks, stride=stride, padding=padding, bias=bias),
                        nn.BatchNorm2d(out_channels),
                        nn.LeakyReLU(slope)
                    ))

        self.n_layers = n_layers
        self.unet_layers = unet_layers

    def __getitem__(self, key):
        return getattr(self, key)

    def forward(self, zs):
        input = self['1'](zs[0][0])
        # print('{}'.format(tuple(zs[0][0].size())), end='')

        for idx in range(1, self.n_layers):
            layer_id = str(idx+1)
            if self.unet_layers and self.unet_layers[idx]:
                # print('--->[{}, {}]'.format(tuple(input.size()), tuple(zs[idx][0].size())), end='')
                input = self[layer_id](input+zs[idx][0])
            else:
                # print('--->{}'.format(tuple(input.size())), end='')
                input = self[layer_id](input)
        # print('--->{}\n'.format(tuple(input.size())))

        return input

class Discriminator(nn.Module):
    def __init__(self, nc, ndf, n_layers, shared_weights=None, ref_net=None, ks=4, stride=2, padding=1, bias=False, slope=0.2, max_c=2048):
        super(Discriminator, self).__init__()

        if not isinstance(ndf, collections.Iterable):
            ndf = [min(ndf*(2**i), max_c)  for i in range(n_layers)]
        assert(len(ndf) == n_layers)
        assert(shared_weights == None or len(shared_weights) == n_layers)

        in_channels = nc
        for idx, out_channels in enumerate(ndf):
            layer_id = str(idx+1)

            if ref_net and shared_weights and shared_weights[idx]:
                self.add_module(layer_id, ref_net[layer_id])
                continue

            if idx == n_layers-1:
                self.add_module(layer_id, 
                    nn.Sequential(
                        nn.Conv2d(in_channels, 1, ks, stride=stride, padding=padding, bias=bias),
                        nn.Sigmoid()
                    ))
            else:
                self.add_module(layer_id, 
                    nn.Sequential(
                        nn.Conv2d(in_channels, out_channels, ks, stride=stride, padding=padding, bias=bias),
                        nn.BatchNorm2d(out_channels),
                        nn.LeakyReLU(slope)
                    ))

            in_channels = out_channels

        self.n_layers = n_layers

    def __getitem__(self, key):
        return getattr(self, key)

    def forward(self, input):
        # print(tuple(input.size()), end='')
        for idx in range(self.n_layers):
            layer_id = str(idx+1)
            input = self[layer_id](input)
            # print('--->', tuple(input.size()), end='')
        # print('\n')

        return input.view(-1, 1)